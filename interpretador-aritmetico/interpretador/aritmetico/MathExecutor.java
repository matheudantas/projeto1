package interpretador.aritmetico;

import java.math.BigDecimal;

public class MathExecutor {
	
	public static Class[] defaultMethodParams = {BigDecimal.class, BigDecimal.class};
	
	public BigDecimal somar(BigDecimal parcela1, BigDecimal parcela2) {
		return parcela1.add(parcela2);
	}
	
	public BigDecimal subtrair(BigDecimal minuendo, BigDecimal subtraendo) {
		return minuendo.subtract(subtraendo);
	}
	
	public BigDecimal multiplicar(BigDecimal fator1, BigDecimal fator2) {
		return fator1.multiply(fator2);
	}
	
	public BigDecimal dividir(BigDecimal dividendo, BigDecimal divisor) {
		return dividendo.divide(divisor);
	}
	
	public BigDecimal exponenciar(BigDecimal base, BigDecimal expoente){
		return base.pow(expoente.toBigInteger().intValue());
	}
	
	public BigDecimal raizNdeX(BigDecimal radicando, BigDecimal expoente) {
		return new BigDecimal(Math.pow(radicando.doubleValue(), 1.0/expoente.toBigInteger().intValue()));
	}
	
}
