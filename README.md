# 1. Objetivo:
* Desenvolver competências:
* Desenvolver software utilizando sockets UDP ou TCP;
* Trabalhar em equipe utilizando controle de versão git em ambiente gitlab;
* Desenvolver software utilizando Java, Python ou outra linguagem;

# 2. Metodologia:
## 2.1) Simulação de trabalho em uma empresa de desenvolvimento de agentes inteligentes, com classificação prévia e objetivos simples usando API de Sockets:
* aranha (crawler): varre a subrede em busca de outros agentes, procurando identificar o IP e a classificação. Quando questionado, fornece somente o último agente localizado. É imune a zumbis.
* respondedor: responde a desafios aritméticos fornecidos por um questionador;
* questionador: fornece desafios aritméticos solicitados e avalia se a resposta está correta;
* zumbi: envia um sinal que faz com que o agente que o contactou se torne um zumbi. Ou seja, não responde e nem questiona desafios aritméticos;
* caçador: envia um sinal que faz com que o zumbi seja preso. É imune ao zumbi;
* curador: envia um sinal que faz com que o zumbi se cure. É imune a zumbis. O único que pode localizar e classificar agentes é a aranha e não pode ser morta.

# 3. Avaliação:

Todos participantes devem colaborar com o desenvolvimento dos agentes, utilizando o git/gitlab como ambiente de colaboração de desenvolvimento. O projeto 1 oficial está localizado neste endereço:

[https://gitlab.com/ad-si-2015-1/projeto1](projeto1)

O professor receberá contribuições ao projeto por meio de "Merge Requests". A avaliação será baseada no critério de  impacto positivo da contribuição para o projeto.

Merge Requests com erros ou que não contribuem com o projeto serão reprovados. Espera-se que cada aluno contribua com pelo menos um MR por semana.

# 4. Teste

