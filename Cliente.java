/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transferencia;

import java.io.*;
import java.net.*;

/**
 *
 * @author Miriã
 */
public class Cliente {
    public static void main(String[] args) throws IOException{
        int filesize = 6022386;
        
        long start = System.currentTimeMillis();
        int bytesRead;
        int current = 0;
        Socket sock = new Socket("127.0.0.1", 123456);
        
        //Recebendo o arquivo.
        byte[] mybytearray = new byte[filesize];
        //byte[] mybytearray = new byte[6022386];
        InputStream is = sock.getInputStream();
        FileOutputStream fos = new FileOutputStream("TesteRecebeArquivo.txt");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        bytesRead = is.read(mybytearray,0,mybytearray.length);
        current = bytesRead;
        //int bytesRead = is.read(mybytearray, 0, mybytearray.length);
        //bos.write(mybytearray, 0, bytesRead);
        //bos.close();
        //sock.close();
        do{
            bytesRead = is.read(mybytearray, current, (mybytearray.length-current));
            if(bytesRead >= 0) current += bytesRead;
        }while (bytesRead > -1);
        
        bos.write(mybytearray, 0, current);
        long end = System.currentTimeMillis();
        System.out.println(end-start);
        bos.close();
        sock.close();
        }
    }